using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public sealed class Player : MonoBehaviour, IEntityObject
    {
        public EcsEntity Entity { get; set; }
    }
}