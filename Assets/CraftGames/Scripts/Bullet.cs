namespace CraftGamesTest
{
    public sealed class Bullet : TriggeredObject
    {
        protected override string CheckTag { get; set; }

        private void Awake()
        {
            CheckTag = Constants.TagAsteroid;
        }
    }
}