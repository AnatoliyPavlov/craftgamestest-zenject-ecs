namespace CraftGamesTest
{
    public sealed class Asteroid : TriggeredObject
    {
        protected override string CheckTag { get; set; }

        private void Awake()
        {
            CheckTag = Constants.TagPlayer;
        }
    }
}