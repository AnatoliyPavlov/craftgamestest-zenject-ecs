using System;

namespace CraftGamesTest
{
    public interface IUserDataService
    {
        public Action<int> ScoreChange { get; set; }
        int Score { get; }
        public Action<int> RecordScoreChange { get; set; }
        int RecordScore { get; }
        void AddScore(int value);
        void ResetScore();
    }
}