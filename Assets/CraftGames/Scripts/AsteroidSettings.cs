﻿using System;
using UnityEngine;

namespace CraftGamesTest
{
    [Serializable]
    public class AsteroidSettings
    {
        public Asteroid Prefab;
        [Space]
        public float SpeedMin = 2.5f;
        public float SpeedMax = 7.5f;
        [Space]
        public float ScaleMin = 0.75f;
        public float ScaleMax = 1.75f;
        [Space]
        public float DamageMin = 5f;
        public float DamageMax = 15f;
        [Space]
        public float HealthMin = 5f;
        public float HealthMax = 15f;
        [Space]
        public float DelayBetweenSpawns = 0.5f;
    }
}