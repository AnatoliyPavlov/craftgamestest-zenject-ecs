using Leopotam.Ecs;
using System.Collections.Generic;
using UnityEngine;

namespace CraftGamesTest
{
    public abstract class TriggeredObject : MonoBehaviour, IEntityObject
    {
        public EcsEntity Entity { get; set; }

        public List<EcsEntity> TriggerEnterEcsEntities = new List<EcsEntity>();

        protected abstract string CheckTag { get; set; }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == CheckTag)
            {
                if (other.gameObject.TryGetComponent(out IEntityObject entity))
                {
                    TriggerEnterEcsEntities.Add(entity.Entity);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == CheckTag)
            {
                if (other.gameObject.TryGetComponent(out IEntityObject entity))
                {
                    TriggerEnterEcsEntities.Remove(entity.Entity);
                }
            }
        }

        private void OnDisable()
        {
            TriggerEnterEcsEntities.Clear();
        }
    }
}