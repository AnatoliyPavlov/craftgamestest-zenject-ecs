using UnityEngine;
using Zenject;

namespace CraftGamesTest
{
    [CreateAssetMenu(menuName = "CraftGameTest/Game Settings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public PlayerSettings PlayerSettings;
        public AsteroidSettings AsteroidSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(AsteroidSettings).IfNotBound();
            Container.BindInstance(PlayerSettings).IfNotBound();
        }
    }
}