using Leopotam.Ecs;

namespace CraftGamesTest
{
    public interface IEntityObject
    {
        EcsEntity Entity { get; set; }
    }
}