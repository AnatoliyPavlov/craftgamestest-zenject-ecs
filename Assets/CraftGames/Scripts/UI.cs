using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CraftGamesTest
{
    public class UI : MonoBehaviour
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private TextMeshProUGUI healthLabel;
        [SerializeField] private TextMeshProUGUI scoreLabel;
        [SerializeField] private TextMeshProUGUI recordScoreLabel;

        private IUserDataService userDataService;
        private Action playButtonClickAction;
        private Action restartButtonClickAction;

        [Inject]
        public void Cunstruct(IUserDataService userDataService)
        {
            this.userDataService = userDataService;
        }

        private void OnEnable()
        {
            playButton.onClick.AddListener(PlayButtonClick);
            restartButton.onClick.AddListener(RestartButtonClick);
            userDataService.ScoreChange += OnScoreChanged;
            userDataService.RecordScoreChange += OnRecordScoreChanged;
        }

        private void PlayButtonClick()
        {
            DisableButtons();
            playButtonClickAction?.Invoke();
        }

        private void RestartButtonClick()
        {
            DisableButtons();
            userDataService.ResetScore();
            restartButtonClickAction?.Invoke();
        }

        private void OnScoreChanged(int score)
        {
            SetScoreLabel(score);
        }

        private void OnRecordScoreChanged(int score)
        {
            SetRecordScoreLabel(score);
        }

        private void OnDisable()
        {
            playButton.onClick.RemoveListener(PlayButtonClick);
            restartButton.onClick.RemoveListener(RestartButtonClick);
            userDataService.ScoreChange -= OnScoreChanged;
            userDataService.ScoreChange -= OnRecordScoreChanged;
        }

        public void ShowStartUI()
        {
            EnablePlayButton(true);
        }

        public void ShowDeathUI()
        {
            EnablePlayButton(false);
        }

        public void SetPlayButtonClickAction(Action action)
        {
            playButtonClickAction = action;
        }

        public void SetRestartButtonClickAction(Action action)
        {
            restartButtonClickAction = action;
        }

        public void SetHealthLabel(float health)
        {
            healthLabel.text = string.Format(Constants.HealthFormat, (int)Math.Ceiling(health));
        }

        private void SetScoreLabel(int score)
        {
            scoreLabel.text = string.Format(Constants.ScoreFormat, score.ToString());
        }

        private void SetRecordScoreLabel(int score)
        {
            recordScoreLabel.text = string.Format(Constants.RecordScoreFormat, score.ToString());
        }

        private void EnablePlayButton(bool isEnable)
        {
            playButton.gameObject.SetActive(isEnable);
            restartButton.gameObject.SetActive(!isEnable);
        }

        private void DisableButtons()
        {
            playButton.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);
        }
    }
}