using Zenject;

namespace CraftGamesTest
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IUserDataService>()
                .To<UserDataService>()
                .AsSingle();
        }
    }
}