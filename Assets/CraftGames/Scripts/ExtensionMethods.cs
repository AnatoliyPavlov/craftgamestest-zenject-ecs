using UnityEngine;

namespace CraftGamesTest
{
    public static class ExtensionMethods
    {
        public static Vector3 ZeroZ(this Vector3 vector3)
        {
            return new Vector3(vector3.x, vector3.y, 0);
        }
    }
}