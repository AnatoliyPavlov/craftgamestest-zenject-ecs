﻿using System;
using UnityEngine;

namespace CraftGamesTest
{
    [Serializable]
    public class PlayerSettings
    {
        public Player PlayerPrefab;
        public Bullet BulletPrefab;
        [Space]
        public Vector3 StartPosition = new Vector3(-7, 0, 0);
        [Space]
        public float PlayerHealth = 100f;
        public float PlayertSpeed = 10f;
        [Space]
        public float CannonDamage = 5f;
        public float CannonReload = 1f;
        public float CannonBulletSpeed = 10f;
        [Space]
        public Material LaserMaterial;
        public float LaserDamage = 2.5f;
    }
}