using System;

namespace CraftGamesTest
{
    public class UserDataService : IUserDataService
    {
        public Action<int> ScoreChange { get; set; }
        public int Score 
        { 
            get { return score; } 

            private set 
            {
                score = value;
                ScoreChange?.Invoke(value);

                if (value > RecordScore)
                {
                    RecordScore = value;
                }
            }
        }
        private int score = 0;

        public Action<int> RecordScoreChange { get; set; }
        public int RecordScore 
        { 
            get { return recordScore; } 

            private set 
            {
                recordScore = value;
                RecordScoreChange?.Invoke(value);
            }
        }
        private int recordScore = 0;


        public void AddScore(int value)
        {
            Score += value;
        }

        public void ResetScore()
        {
            Score = 0;
        }
    }
}