using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class RuntimeData
    {
        public bool IsNeedInitPlayer = false;
        public bool GameEnd = true;
        public EcsEntity PlayerEntity;
    }
}