namespace CraftGamesTest
{
    public static class Constants
    {
        public const string TagAsteroid = "Asteroid";
        public const string TagPlayer = "Player";
        public const float EndLeftX = -10f;
        public const float EndRightX = 10f;
        public const float EndTopY = 5f;
        public const float EndBotY = -5f;
        public const int VisionDistance = 20;
        public const string HealthFormat = "Health: {0}";
        public const string ScoreFormat = "Score: {0}";
        public const string RecordScoreFormat = "Record: {0}";
        public const float LaserLineRendererWidth = 0.25f;
    }
}