namespace CraftGamesTest
{
    public struct AsteroidComponent
    {
        public float Damage;
        public Asteroid Asteroid;
    }
}