namespace CraftGamesTest
{
    public struct BulletComponent
    {
        public float Damage;
        public Bullet Bullet;
    }
}