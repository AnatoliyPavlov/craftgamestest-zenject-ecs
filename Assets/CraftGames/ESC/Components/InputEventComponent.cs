using UnityEngine;

namespace CraftGamesTest
{
    public struct InputEventComponent
    {
        public Vector2 WasdDirection;
        public bool MouseLeftClick;
        public bool MouseRightClick;
    }
}