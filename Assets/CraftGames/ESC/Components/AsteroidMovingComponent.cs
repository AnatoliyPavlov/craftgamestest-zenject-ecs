using UnityEngine;

namespace CraftGamesTest
{
    public struct AsteroidMovingComponent
    {
        public float Speed;
        public Vector3 Target;
    }
}