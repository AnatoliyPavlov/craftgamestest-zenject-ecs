using UnityEngine;

namespace CraftGamesTest
{
    public struct MovableComponent
    {
        public Transform Transform;
        public float Speed;
    }
}