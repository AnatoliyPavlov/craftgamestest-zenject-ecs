using Leopotam.Ecs;

namespace CraftGamesTest
{
    public struct DamageEvent
    {
        public EcsEntity Target;
        public float Value;
    }
}