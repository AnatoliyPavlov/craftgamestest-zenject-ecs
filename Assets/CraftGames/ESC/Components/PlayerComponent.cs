using UnityEngine;

namespace CraftGamesTest
{
    public struct PlayerComponent
    {
        public float CannonReload;
        public LineRenderer LaserLineRenderer;
    }
}