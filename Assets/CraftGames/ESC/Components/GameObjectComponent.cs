using UnityEngine;

namespace CraftGamesTest
{
    public struct GameObjectComponent
    {
        public GameObject GameObject;
    }
}