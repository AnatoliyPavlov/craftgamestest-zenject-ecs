using UnityEngine;

namespace CraftGamesTest
{
    public struct MoveToPositionComponent
    {
        public Vector3 Target;
    }
}