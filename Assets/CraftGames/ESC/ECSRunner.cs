using Leopotam.Ecs;
using UnityEngine;
using Zenject;

namespace CraftGamesTest
{
    public class ECSRunner : MonoBehaviour
    {
        [SerializeField] private UI ui;

        private EcsWorld world;
        private EcsSystems systems;
        private EcsSystems updateSystems;
        private EcsSystems fixedUpdateSystems;

        private RuntimeData runTimeData;

        private AsteroidSettings asteroidSettings;
        private PlayerSettings playerSettings;
        private IUserDataService userDataService;

        [Inject]
        public void Construct(AsteroidSettings asteroidSettings, PlayerSettings playerSettings, IUserDataService userDataService)
        {
            this.asteroidSettings = asteroidSettings;
            this.playerSettings = playerSettings;
            this.userDataService = userDataService;
        }

        private void Start()
        {
            world = new EcsWorld();
            systems = new EcsSystems(world);
            updateSystems = new EcsSystems(world);
            fixedUpdateSystems = new EcsSystems(world);

            runTimeData = new RuntimeData();

            AddJustInitSystems();
            AddUpdatebleSystems();
            AddFixedUpdatebleSystems();

            systems.Init();
            updateSystems.Init();
            fixedUpdateSystems.Init();
        }

        private void Update()
        {
            updateSystems.Run();
        }

        private void FixedUpdate()
        {
            fixedUpdateSystems.Run();
        }

        private void OnDestroy()
        {
            systems.Destroy();
            updateSystems.Destroy();
            fixedUpdateSystems.Destroy();

            world.Destroy();
        }

        private void AddJustInitSystems()
        {
            systems
                .Add(new UIInitSystem())
                .Inject(ui)
                .Inject(runTimeData);
        }

        private void AddUpdatebleSystems()
        {
            updateSystems
                .Add(new PlayerCreateSystem())
                .Add(new PlayerInputSystem())
                .Add(new PlayerDeathSystem())
                .Add(new AsteroidCreateSystem())
                .Add(new AsteroidDeathSystem())
                .Add(new DamageSystem())
                .Inject(playerSettings)
                .Inject(runTimeData)
                .Inject(ui)
                .Inject(userDataService)
                .Inject(asteroidSettings);
        }

        private void AddFixedUpdatebleSystems()
        {
            fixedUpdateSystems
                .Add(new BulletTriggerSystem())
                .Add(new AsteroidTriggerSystem())
                .Add(new PlayerMoveSystem())
                .Add(new PlayerCannonFireSystem())
                .Add(new PlayerLaserFireSystem())
                .Add(new BulletMoveSystem())
                .Add(new AsteroidMoveSystem())
                .Inject(runTimeData)
                .Inject(playerSettings);
        }
    }
}