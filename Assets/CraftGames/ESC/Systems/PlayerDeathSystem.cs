using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class PlayerDeathSystem : IEcsRunSystem
    {
        private EcsFilter<PlayerComponent, DeathEventComponent, GameObjectComponent> deadPlayers;
        private RuntimeData runtimeData;
        private UI ui;

        public void Run()
        {
            foreach (var i in deadPlayers)
            {
                ui.ShowDeathUI();

                runtimeData.GameEnd = true;

                deadPlayers.GetEntity(i).Del<DeathEventComponent>();
            }
        }
    }
}