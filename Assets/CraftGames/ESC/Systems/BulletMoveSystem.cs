using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class BulletMoveSystem : IEcsRunSystem
    {
        private readonly EcsFilter<BulletComponent, GameObjectComponent, MovableComponent, MoveToPositionComponent> bulletsMoveFilter = null;

        public void Run()
        {
            foreach (var i in bulletsMoveFilter)
            {
                ref var gameObjectComponent = ref bulletsMoveFilter.Get2(i);

                if (!gameObjectComponent.GameObject.activeInHierarchy) { continue; }

                ref var bulletComponent = ref bulletsMoveFilter.Get1(i);
                ref var movableComponent = ref bulletsMoveFilter.Get3(i);
                ref var moveToPositionComponent = ref bulletsMoveFilter.Get4(i);

                if (Vector3.Distance(movableComponent.Transform.position, moveToPositionComponent.Target) > 0.1)
                {
                    movableComponent.Transform.position = Vector3.MoveTowards(movableComponent.Transform.position, moveToPositionComponent.Target, movableComponent.Speed * Time.deltaTime);
                }
                else
                {
                    gameObjectComponent.GameObject.SetActive(false);
                }
            }
        }
    }
}