using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class UIInitSystem : IEcsInitSystem
    {
        private readonly EcsFilter<GameObjectComponent> goFilter = null;

        private UI ui;
        private RuntimeData runtimeData;

        public void Init()
        {
            SetUIPlayButtonClick();
            SetUIRestartButtonClick();
        }

        private void SetUIPlayButtonClick()
        {
            ui.SetPlayButtonClickAction(() =>
            {
                runtimeData.IsNeedInitPlayer = true;
                runtimeData.GameEnd = false;
            });
        }

        private void SetUIRestartButtonClick()
        {
            ui.SetRestartButtonClickAction(() =>
            {
                foreach (var i in goFilter)
                {
                    ref var go = ref goFilter.Get1(i);
                    go.GameObject.SetActive(false);
                }
                runtimeData.IsNeedInitPlayer = true;
                runtimeData.GameEnd = false;
            });
        }
    }
}