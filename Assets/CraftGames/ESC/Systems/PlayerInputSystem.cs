using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class PlayerInputSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerComponent, InputEventComponent> inputEventsFilter = null;

        public void Run()
        {
            var x = Input.GetAxis("Horizontal");
            var y = Input.GetAxis("Vertical");
            var mouseLeftClick = Input.GetAxis("Fire1");
            var mouseRightClick = Input.GetAxis("Fire2");

            foreach (var i in inputEventsFilter)
            {
                ref var inputEvent = ref inputEventsFilter.Get2(i);

                inputEvent.WasdDirection = new Vector2(x, y);
                inputEvent.MouseLeftClick = mouseLeftClick > 0;
                inputEvent.MouseRightClick = mouseRightClick > 0;
            }
        }
    }
}