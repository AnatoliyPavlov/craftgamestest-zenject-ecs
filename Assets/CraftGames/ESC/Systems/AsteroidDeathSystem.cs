using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class AsteroidDeathSystem : IEcsRunSystem
    {
        private EcsFilter<AsteroidComponent, DeathEventComponent> deadAsteroids;

        private IUserDataService userDataService;

        public void Run()
        {
            foreach (var i in deadAsteroids)
            {
                userDataService.AddScore(1);

                deadAsteroids.GetEntity(i).Del<DeathEventComponent>();
            }
        }
    }
}