using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class PlayerLaserFireSystem : IEcsRunSystem
    {
        private readonly EcsWorld world = null;

        private readonly EcsFilter<PlayerComponent, InputEventComponent, MovableComponent> playerFilter = null;

        private PlayerSettings playerSettings;
        private RuntimeData runtimeData;

        public void Run()
        {
            if (runtimeData.GameEnd) { return; }

            foreach (var i in playerFilter)
            {
                ref var playerComponent = ref playerFilter.Get1(i);
                ref var playerInputComponent = ref playerFilter.Get2(i);
                ref var playerMovableComponent = ref playerFilter.Get3(i);

                if (playerInputComponent.MouseRightClick)
                {
                    playerComponent.LaserLineRenderer.enabled = true;

                    playerComponent.LaserLineRenderer.SetPosition(0, playerMovableComponent.Transform.position);

                    Vector3 mouseClickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition).ZeroZ();
                    Vector3 direction = mouseClickPosition - playerMovableComponent.Transform.position;

                    //Check is fisrt raycast hit in asteroid and make damage event
                    RaycastHit hit;
                    if (Physics.Raycast(playerMovableComponent.Transform.position, direction, out hit, Constants.VisionDistance))
                    {
                        if (hit.collider.tag == Constants.TagAsteroid)
                        {
                            playerComponent.LaserLineRenderer.SetPosition(1, hit.point);

                            if (hit.collider.gameObject.TryGetComponent(out IEntityObject entety))
                            {
                                CreateDamageEvent(entety);

                                return;
                            }
                        }
                    }

                    //Check is fisrt raycast hit in non-asteroid and check other raycasts and make damage event
                    var hits = Physics.RaycastAll(playerMovableComponent.Transform.position, direction, Constants.VisionDistance);
                    for (var j = 0; j < hits.Length; j++)
                    {
                        if (hits[j].transform.tag == Constants.TagAsteroid)
                        {
                            playerComponent.LaserLineRenderer.SetPosition(1, hits[j].point);

                            if (hits[j].collider.gameObject.TryGetComponent(out IEntityObject entety))
                            {
                                CreateDamageEvent(entety);

                                return;
                            }
                        }
                    }

                    Vector3 targetPosition = playerMovableComponent.Transform.position + direction.normalized * Constants.VisionDistance;

                    playerComponent.LaserLineRenderer.SetPosition(1, targetPosition);
                }
                else
                {
                    playerComponent.LaserLineRenderer.enabled = false;
                }
            }
        }

        private void CreateDamageEvent(IEntityObject entety)
        {
            ref var damageEvent = ref world.NewEntity().Get<DamageEvent>();
            damageEvent.Target = entety.Entity;
            damageEvent.Value = playerSettings.LaserDamage * Time.fixedDeltaTime;
        }
    }
}