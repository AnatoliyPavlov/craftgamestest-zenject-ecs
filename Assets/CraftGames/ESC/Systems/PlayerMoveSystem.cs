using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class PlayerMoveSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerComponent, InputEventComponent, MovableComponent> playerMoveFilter = null;

        private RuntimeData runtimeData;

        public void Run()
        {
            if (runtimeData.GameEnd) { return; }

            foreach (var i in playerMoveFilter)
            {
                ref var inputComponent = ref playerMoveFilter.Get2(i);
                ref var movableComponent = ref playerMoveFilter.Get3(i);

                var addPosition = (Vector3)inputComponent.WasdDirection * Time.deltaTime * movableComponent.Speed;

                if (Mathf.Abs(movableComponent.Transform.position.x + addPosition.x) > 9)
                {
                    addPosition.x = 0;
                }

                if (Mathf.Abs(movableComponent.Transform.position.y + addPosition.y) > 5)
                {
                    addPosition.y = 0;
                }

                movableComponent.Transform.position += addPosition;
            }
        }
    }
}