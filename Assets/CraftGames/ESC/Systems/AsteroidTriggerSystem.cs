using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class AsteroidTriggerSystem : IEcsRunSystem
    {
        private readonly EcsWorld world = null;

        private readonly EcsFilter<AsteroidComponent, GameObjectComponent> asteroidsFilter = null;

        public void Run()
        {
            foreach (var i in asteroidsFilter)
            {
                ref var gameObjectComponent = ref asteroidsFilter.Get2(i);

                if (!gameObjectComponent.GameObject.activeInHierarchy) { continue; }

                ref var asteroidComponent = ref asteroidsFilter.Get1(i);

                var triggerEnterEcsEntities = asteroidComponent.Asteroid.TriggerEnterEcsEntities;

                if (triggerEnterEcsEntities.Count > 0)
                {
                    for (int j = 0; j < triggerEnterEcsEntities.Count; j++)
                    {
                        ref var damageEvent = ref world.NewEntity().Get<DamageEvent>();
                        damageEvent.Target = triggerEnterEcsEntities[j];
                        damageEvent.Value = asteroidComponent.Damage;
                    }

                    gameObjectComponent.GameObject.SetActive(false);
                }
            }
        }
    }
}