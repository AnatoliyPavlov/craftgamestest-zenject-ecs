using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class PlayerCreateSystem : IEcsRunSystem
    {
        private readonly EcsWorld world = null;

        private readonly EcsFilter<PlayerComponent, GameObjectComponent, HealthComponent, MovableComponent> playerFilter = null;

        private PlayerSettings playerSettings;
        private RuntimeData runtimeData;
        private UI ui;

        public void Run()
        {
            if (!runtimeData.IsNeedInitPlayer) { return; }

            runtimeData.IsNeedInitPlayer = false;
            ui.SetHealthLabel(playerSettings.PlayerHealth);

            foreach (var i in playerFilter)
            {
                ref var gameObjectComponent = ref playerFilter.Get2(i);
                ref var healthComponent = ref playerFilter.Get3(i);
                ref var movableComponent = ref playerFilter.Get4(i);

                SetParametersPlayer(ref healthComponent, ref movableComponent);

                gameObjectComponent.GameObject.SetActive(true);

                return;
            }

            var player = world.NewEntity();
            runtimeData.PlayerEntity = player;
            ref var playerComponent = ref player.Get<PlayerComponent>();
            ref var newGameObjectComponent = ref player.Get<GameObjectComponent>();
            ref var newHealthComponent = ref player.Get<HealthComponent>();
            player.Get<InputEventComponent>();
            ref var newMovableComponent = ref player.Get<MovableComponent>();

            InitPlayer(ref player, ref playerComponent, ref newGameObjectComponent, ref newMovableComponent);

            SetParametersPlayer(ref newHealthComponent, ref newMovableComponent);
        }

        private void InitPlayer(ref EcsEntity playerEntity, ref PlayerComponent playerComponent, ref GameObjectComponent gameObjectComponent, ref MovableComponent movableComponent)
        {
            var spawnedPlayerPrefab = GameObject.Instantiate(playerSettings.PlayerPrefab, playerSettings.StartPosition, Quaternion.Euler(0, 45, 45));
            spawnedPlayerPrefab.Entity = playerEntity;
            playerComponent.LaserLineRenderer = spawnedPlayerPrefab.gameObject.AddComponent<LineRenderer>();
            playerComponent.LaserLineRenderer.startWidth = Constants.LaserLineRendererWidth;
            playerComponent.LaserLineRenderer.endWidth = Constants.LaserLineRendererWidth;
            playerComponent.LaserLineRenderer.material = playerSettings.LaserMaterial;
            gameObjectComponent.GameObject = spawnedPlayerPrefab.gameObject;
            movableComponent.Speed = playerSettings.PlayertSpeed;
            movableComponent.Transform = spawnedPlayerPrefab.transform;
        }

        private void SetParametersPlayer(ref HealthComponent healthComponent, ref MovableComponent movableComponent)
        {
            healthComponent.Value = playerSettings.PlayerHealth;
            movableComponent.Transform.position = playerSettings.StartPosition;
        }
    }
}