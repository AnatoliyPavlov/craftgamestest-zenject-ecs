using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class DamageSystem : IEcsRunSystem
    {
        private EcsFilter<DamageEvent> damageEvents;

        private UI ui;
        private RuntimeData runtimeData;

        public void Run()
        {
            foreach (var i in damageEvents)
            {
                ref var damageEvent = ref damageEvents.Get1(i);
                ref var healthComponent = ref damageEvent.Target.Get<HealthComponent>();
                ref var gameObjectComponent = ref damageEvent.Target.Get<GameObjectComponent>();

                healthComponent.Value -= damageEvent.Value;

                if (healthComponent.Value <= 0)
                {
                    healthComponent.Value = 0;

                    damageEvent.Target.Get<DeathEventComponent>();
                    gameObjectComponent.GameObject.SetActive(false);
                }

                if (damageEvent.Target == runtimeData.PlayerEntity)
                {
                    ui.SetHealthLabel(healthComponent.Value);
                }

                damageEvents.GetEntity(i).Destroy();
            }
        }
    }
}