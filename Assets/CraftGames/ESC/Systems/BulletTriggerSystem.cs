using Leopotam.Ecs;

namespace CraftGamesTest
{
    public class BulletTriggerSystem : IEcsRunSystem
    {
        private readonly EcsWorld world = null;

        private readonly EcsFilter<BulletComponent, GameObjectComponent> bulletsFilter = null;

        public void Run()
        {
            foreach (var i in bulletsFilter)
            {
                ref var gameObjectComponent = ref bulletsFilter.Get2(i);

                if (!gameObjectComponent.GameObject.activeInHierarchy) { continue; }

                ref var bulletComponent = ref bulletsFilter.Get1(i);

                var triggerEnterEcsEntities = bulletComponent.Bullet.TriggerEnterEcsEntities;

                if (triggerEnterEcsEntities.Count > 0)
                {
                    for (int j = 0; j < triggerEnterEcsEntities.Count; j++)
                    {
                        ref var damageEvent = ref world.NewEntity().Get<DamageEvent>();
                        damageEvent.Target = triggerEnterEcsEntities[j];
                        damageEvent.Value = bulletComponent.Damage;
                    }

                    gameObjectComponent.GameObject.SetActive(false);
                }
            }
        }
    }
}