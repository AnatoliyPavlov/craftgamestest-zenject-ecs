using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class PlayerCannonFireSystem : IEcsRunSystem
    {
        private readonly EcsWorld world = null;

        private readonly EcsFilter<PlayerComponent, InputEventComponent, MovableComponent> playerFilter = null;
        private readonly EcsFilter<BulletComponent, GameObjectComponent, MovableComponent, MoveToPositionComponent> bulletsFilter = null;

        private PlayerSettings playerSettings;
        private RuntimeData runtimeData;

        public void Run()
        {
            if (runtimeData.GameEnd) { return; }

            foreach (var i in playerFilter)
            {
                ref var playerComponent = ref playerFilter.Get1(i);
                ref var playerInputComponent = ref playerFilter.Get2(i);
                ref var playerMovableComponent = ref playerFilter.Get3(i);

                playerComponent.CannonReload += Time.deltaTime;

                if (playerInputComponent.MouseLeftClick && playerComponent.CannonReload > playerSettings.CannonReload)
                {
                    playerComponent.CannonReload = 0;

                    foreach (var j in bulletsFilter)
                    {
                        ref var bulletGameObjectComponent = ref bulletsFilter.Get2(j);

                        if (!bulletGameObjectComponent.GameObject.activeInHierarchy)
                        {
                            ref var bulletMovableComponent = ref bulletsFilter.Get3(j);
                            ref var bulletMoveToPositionComponent = ref bulletsFilter.Get4(j);

                            SetParametersBullet(ref bulletMovableComponent, ref bulletMoveToPositionComponent, ref playerMovableComponent);

                            bulletGameObjectComponent.GameObject.SetActive(true);

                            return;
                        }
                    }

                    //Create new Entity
                    var bulletEntity = world.NewEntity();
                    ref var bulletComponent = ref bulletEntity.Get<BulletComponent>();
                    ref var newBulletGameObjectComponent = ref bulletEntity.Get<GameObjectComponent>();
                    ref var newBulletMovableComponent = ref bulletEntity.Get<MovableComponent>();
                    ref var newBulletMoveToPositionComponent = ref bulletEntity.Get<MoveToPositionComponent>();

                    InitBullet(ref bulletEntity, ref bulletComponent, ref newBulletGameObjectComponent, ref newBulletMovableComponent, ref playerMovableComponent);

                    SetParametersBullet(ref newBulletMovableComponent, ref newBulletMoveToPositionComponent, ref playerMovableComponent);
                }
            }
        }

        private void InitBullet(ref EcsEntity bulletEntity, ref BulletComponent bulletComponent, ref GameObjectComponent bulletGameObjectComponent, ref MovableComponent bulletMovableComponent, ref MovableComponent playerMovableComponent)
        {
            var spawnedBulletPrefab = GameObject.Instantiate(playerSettings.BulletPrefab, playerMovableComponent.Transform.position, Quaternion.Euler(0, 45, 45));
            spawnedBulletPrefab.Entity = bulletEntity;
            bulletComponent.Damage = playerSettings.CannonDamage;
            bulletComponent.Bullet = spawnedBulletPrefab.GetComponent<Bullet>();
            bulletGameObjectComponent.GameObject = spawnedBulletPrefab.gameObject;
            bulletMovableComponent.Speed = playerSettings.CannonBulletSpeed;
            bulletMovableComponent.Transform = spawnedBulletPrefab.gameObject.transform;
        }

        private void SetParametersBullet(ref MovableComponent bulletMovableComponent, ref MoveToPositionComponent moveToPositionComponent, ref MovableComponent playerMovableComponent)
        {
            bulletMovableComponent.Transform.position = playerMovableComponent.Transform.position;
            Vector3 mouseClickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition).ZeroZ();
            Vector3 direction = mouseClickPosition - bulletMovableComponent.Transform.position;
            Vector3 targetPosition = bulletMovableComponent.Transform.position + direction.normalized * Constants.VisionDistance;
            moveToPositionComponent.Target = targetPosition;
        }
    }
}