using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public class AsteroidMoveSystem : IEcsRunSystem
    {
        private readonly EcsFilter<AsteroidComponent, GameObjectComponent, MovableComponent, MoveToPositionComponent> asteroidMoveFilter = null;

        public void Run()
        {
            foreach (var i in asteroidMoveFilter)
            {
                ref var asteroidComponent = ref asteroidMoveFilter.Get1(i);
                ref var gameObjectComponent = ref asteroidMoveFilter.Get2(i);
                ref var movableComponent = ref asteroidMoveFilter.Get3(i);
                ref var moveToPositionComponent = ref asteroidMoveFilter.Get4(i);

                if (!gameObjectComponent.GameObject.activeInHierarchy) { continue; }

                if (Vector3.Distance(movableComponent.Transform.position, moveToPositionComponent.Target) > 0.1)
                {
                    movableComponent.Transform.position = Vector3.MoveTowards(movableComponent.Transform.position, moveToPositionComponent.Target, movableComponent.Speed * Time.deltaTime);
                }
                else
                {
                    gameObjectComponent.GameObject.SetActive(false);
                }
            }
        }
    }
}