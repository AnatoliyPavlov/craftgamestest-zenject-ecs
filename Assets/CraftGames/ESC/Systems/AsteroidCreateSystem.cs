using Leopotam.Ecs;
using UnityEngine;

namespace CraftGamesTest
{
    public partial class AsteroidCreateSystem : IEcsRunSystem
    {
        private readonly EcsWorld world = null;

        private readonly EcsFilter<AsteroidComponent, GameObjectComponent, HealthComponent, MovableComponent, MoveToPositionComponent> asteroidsFilter = null;

        private AsteroidSettings asteroidSettings;
        private RuntimeData runtimeData;
        private float spawnTimer = 0;

        public void Run()
        {
            if (runtimeData.GameEnd) { return; }

            spawnTimer += Time.deltaTime;

            if (spawnTimer > asteroidSettings.DelayBetweenSpawns)
            {
                spawnTimer = 0;

                CreateAsteroid();
            }
        }

        private void CreateAsteroid()
        {
            foreach (var i in asteroidsFilter)
            {
                ref var asteroidComponent = ref asteroidsFilter.Get1(i);
                ref var gameoObjectComponent = ref asteroidsFilter.Get2(i);
                ref var healthComponent = ref asteroidsFilter.Get3(i);
                ref var movableComponent = ref asteroidsFilter.Get4(i);
                ref var moveToPositionComponent = ref asteroidsFilter.Get5(i);

                if (!gameoObjectComponent.GameObject.activeInHierarchy)
                {
                    SetParametersAsteroid(ref asteroidComponent, ref healthComponent, ref movableComponent, ref moveToPositionComponent);

                    gameoObjectComponent.GameObject.SetActive(true);

                    return;
                }
            }

            //Create new Entity
            var asteroidEntity = world.NewEntity();
            ref var newAsteroidComponent = ref asteroidEntity.Get<AsteroidComponent>();
            ref var newHealth = ref asteroidEntity.Get<HealthComponent>();
            ref var newGameObjectComponent = ref asteroidEntity.Get<GameObjectComponent>();
            ref var newMovableComponent = ref asteroidEntity.Get<MovableComponent>();
            ref var newMoveToPositionComponent = ref asteroidEntity.Get<MoveToPositionComponent>();

            InitAsteroid(ref asteroidEntity, ref newAsteroidComponent, ref newGameObjectComponent, ref newMovableComponent);

            SetParametersAsteroid(ref newAsteroidComponent, ref newHealth, ref newMovableComponent, ref newMoveToPositionComponent);
        }

        private void InitAsteroid(ref EcsEntity asteroidEntity, ref AsteroidComponent asteroidComponent, ref GameObjectComponent gameObjectComponent, ref MovableComponent movableComponent)
        {
            var spawnedAsteroidPrefab = GameObject.Instantiate(asteroidSettings.Prefab, Vector3.zero, Quaternion.identity);
            spawnedAsteroidPrefab.Entity = asteroidEntity;
            asteroidComponent.Asteroid = spawnedAsteroidPrefab.GetComponent<Asteroid>();
            gameObjectComponent.GameObject = spawnedAsteroidPrefab.gameObject;
            movableComponent.Transform = spawnedAsteroidPrefab.gameObject.transform;
        }

        private void SetParametersAsteroid(ref AsteroidComponent asteroidComponent, ref HealthComponent healthComponent, ref MovableComponent movableComponent, ref MoveToPositionComponent moveToPositionComponent)
        {
            asteroidComponent.Damage = Random.Range(asteroidSettings.DamageMin, asteroidSettings.DamageMax);
            healthComponent.Value = Random.Range(asteroidSettings.HealthMin, asteroidSettings.HealthMax);
            movableComponent.Speed = Random.Range(asteroidSettings.SpeedMin, asteroidSettings.SpeedMax);
            movableComponent.Transform.position = new Vector2(Constants.EndRightX, Random.Range(Constants.EndBotY, Constants.EndTopY));
            movableComponent.Transform.localScale = Vector3.one * Random.Range(asteroidSettings.ScaleMin, asteroidSettings.ScaleMax);
            moveToPositionComponent.Target = new Vector3(Constants.EndLeftX, Random.Range(Constants.EndBotY, Constants.EndTopY), 0);
        }
    }
}